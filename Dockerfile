FROM ubuntu as dev

RUN mkdir -p /work
WORKDIR /work
COPY ./build/ordmatch /work
COPY ./ordMatch.conf /work/OrdMatch.conf
COPY ./ordMatch.db /work
COPY ./ordMatchLog.db /work
COPY ./lib/libodbc.so.2 /lib/x86_64-linux-gnu
RUN chmod +x ./ordmatch
ENTRYPOINT ["./ordmatch", "-rematch"]

FROM node:10 as tester
RUN mkdir /work
WORKDIR /work

COPY package*.json ./
RUN pwd
RUN ls -l
RUN npm install
