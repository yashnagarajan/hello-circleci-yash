#--- Makefile ----

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOFORMAT=$(GOCMD) fmt
BINARY_NAME=ordmatch
BINARY_LINUX=$(BINARY_NAME)
BINARY_WINDOWS=$(BINARY_NAME).exe
BINARY_DIR=cmd

VERSION=$(shell git describe --abbrev=0 --tags)
BUILD=$(shell date +%FT%T%z)
LDFLAGS=-ldflags "-X main.Version=$(VERSION) -X main.Build=$(BUILD)"
STATIC=-ldflags "-linkmode external -extldflags -static"


all: show-ver arch build

ifeq "$(shell uname -s)" "Linux"
arch:
	@echo "Bulding under: $(shell uname -s)"
build:
	$(GOBUILD) ${LDFLAGS} -o $(BINARY_LINUX) -v
static-linux:
	$(GOBUILD) ${LDFLAGS} ${STATIC} -o $(BINARY_LINUX) -v

build-windows:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 $(GOBUILD) ${LDFLAGS} -o $(BINARY_WINDOWS) -v

docker-build:
	docker run --rm -it -v "$(GOPATH)":/go -w <registry> golang:latest go build -o "$(BINARY_LINUX)" -v

build-linux64:
	CGO_ENABLED=1 GOOS=linux GOARCH=amd64 $(GOBUILD) ${LDFLAGS} -o ./$(BINARY_DIR)/$(BINARY_NAME)_linux64 -v
build-linux32:
	CGO_ENABLED=1 GOOS=linux GOARCH=386 $(GOBUILD) ${LDFLAGS} -o ./$(BINARY_DIR)/$(BINARY_NAME)_linux32 -v
build-windows32:
    CGO_ENABLED=1 GOOS=windows GOARCH=386 CC=i686-w64-mingw32-gcc CXX=i686-w64-mingw32-g++ $(GOBUILD) ${LDFLAGS} -o ./$(BINARY_DIR)/$(BINARY_NAME)32.exe -v
build-windows64:
    CGO_ENABLED=1 GOOS=windows GOARCH=amd64 CC=x86_64-w64-mingw32-gcc CXX=x86_64-w64-mingw32-g++ $(GOBUILD) ${LDFLAGS} -o ./$(BINARY_DIR)/$(BINARY_NAME)64.exe -v

else
arch:
	@echo "Bulding under: Not Linux (assuming Windows) $(OS)"
build:
	$(GOBUILD) ${LDFLAGS} -o $(BINARY_WINDOWS) -v

build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) ${LDFLAGS} -o $(BINARY_LINUX) -v



static-linux:
	@echo "Building static $(BINARY_NAME) - only when running under Linux"

endif

show-ver:
	@echo "Bulding Ver: ${VERSION} Date: ${BUILD} "

show-ver-tag:
	@echo "${VERSION}"

show-ver-no-seq:
	@echo "${VERSION}" | cut -f1 -d"_"
	
test:
	$(GOTEST) -v ./...

clean:
	$(GOCLEAN)
	rm -f $(BINARY_WINDOWS)
	rm -f $(BINARY_LINUX)

run:
	$(GOBUILD) -o $(BINARY_NAME) -v ./...
	./$(BINARY_NAME)

deps:
	$(GOGET) "github.com/fatih/color"
